<?php
	ini_set('max_execution_time', 50); 
	
	$line = array();
	$result = array();
	$drop=0;
	$len =0;

	function checkResult($str)
	{
		$res = explode(',',$str);
		$len= count($res);
		if($len==1)return;
		$drop = $res[0]- $res[$len -1];
		if($GLOBALS["len"]<$len){
			$GLOBALS["result"] = array();$GLOBALS["result"][0] = $str;$GLOBALS["drop"] = $drop;$GLOBALS["len"]=$len;
		}
		else if($GLOBALS["len"]==$len){
			if($GLOBALS["drop"]<$drop){$GLOBALS["result"] = array();$GLOBALS["result"][0] = $str;$GLOBALS["drop"] = $drop;$GLOBALS["len"]=$len;}
			else if($GLOBALS["drop"]<$drop){$GLOBALS["result"][count($GLOBALS["result"])] = $str; $GLOBALS["drop"] = $drop;$GLOBALS["len"]=$len;}
		}

	}
	/**
	*	Save Every Line From The text
	*	$str -> Every Line that sent to Function 
	*	$index -> Number of line is going to be processed
	* 	$column -> The criteria column that should be accomplished
	*/
	function saveEveryLine($str,$index,$column)
	{
		$GLOBALS["line"][$index] = explode(" ",$str);
		if(count($GLOBALS["line"][$index])!=$column)
			return 1;
	}
	/**
	*	Recursion To All Column From a point to Skiing Down Hill
	* 	$row -> THe Criteria Row 
	*	$column -> The Criteria Column
	* 	$i, $j -> The point that is goint to be processed
	* 	$str -> The Result that save which value has been through
	* 	$state -> The state that is going to be condition when the recursion done
	* 	When State is zero at the end of the function that's mean it's already at the stack point
	* 	The recursion working through all direction 
	*/
	function recurToAllColumn($row,$column,$i,$j,$str,$state)
	{
		$line = $GLOBALS["line"];
		if($i>0 && intval($line[$i-1][$j])<intval($line[$i][$j])){recurToAllColumn($row,$column,$i-1,$j,$str.','.$line[$i-1][$j],0); $state = 1;}
		if($j+1<$column && intval($line[$i][$j+1])<intval($line[$i][$j])){recurToAllColumn($row,$column,$i,$j+1,$str.','.$line[$i][$j+1],0); $state = 1;}
		if($i+1<$row && intval($line[$i+1][$j])<intval($line[$i][$j])){recurToAllColumn($row,$column,$i+1,$j,$str.','.$line[$i+1][$j],0);$state = 1;}
		if($j>0 && intval($line[$i][$j-1])<intval($line[$i][$j])){recurToAllColumn($row,$column,$i,$j-1,$str.','.$line[$i][$j-1],0); $state = 1;}
		if($state ==0 ) checkResult($str);
	}

	/**
	*	Function to check all cell to be processed
	* 	$row => THe Criteria Row
	* 	$column => The Criteria Column
	*/
	function checkAllLine($row,$column)
	{
		$line = $GLOBALS["line"];
		$i=$j=0;
		for($i=0;$i<$row;$i++)
		{
			for($j=0;$j<$column;$j++)
			{
				recurToAllColumn($row,$column,$i,$j,$line[$i][$j],0);
			}
		}
	}
	/**
	*	Open The Text File called file.txt
	* 	Scan the file that's the first row have to be contained with 2 integer ( Row, Column)
 	*/
	$fp = fopen("file.txt","r");
	if (fscanf($fp,"%d\t%d",$row,$column)==2)
	{
		$index = 0;
		$fail = 0 ;
		while(!feof($fp))
		{
			$str = fgets($fp);
			if(saveEveryLine($str,$index,$column)==1 || $index >= $row)
			{
				$fail = 1;
				break;
			}
			$index = $index + 1;

		}
		if($index!=$row)
			$fail = 2;
		if($fail==0)
		{
			/**
			* 	When scan the file succeed
			*	Check every cell in the row
			*	and then Print the result
			*/
			checkAllLine($row,$column);
			print_r($GLOBALS["result"]);
			$result = explode(',',$GLOBALS["result"][0]);
			echo "</br>Length : ".count($result)."</br>";
			echo "Drop : ".strval(intval($result[0])- intval($result[count($result)-1]));
			echo "</br>".$result[0]." ".$result[count($result)-1];
		}
		else if($fail==1)
			echo "Wrong Format File Wrong,exactly at Row ".($index+1);
		else if($fail==2)
			echo "Wrong Format File , The Row not matched with criteria";
	}

?>